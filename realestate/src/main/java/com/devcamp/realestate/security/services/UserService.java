package com.devcamp.realestate.security.services;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestate.models.User;
import com.devcamp.realestate.models.User1;
import com.devcamp.realestate.repository.User1Repository;
import com.devcamp.realestate.repository.UserRepository;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    User1Repository user1Repository;

    public User createUser(User pUser) {
        return userRepository.save(pUser);
    }

    public User1 createUser1(User1 pUser) {
        return user1Repository.save(pUser);
    }

    public Map<String, BigInteger> getUserTotal() {
        List<Object[]> userTotal = userRepository.getUserTotal();
        return convertToMap(userTotal);
    }

    public Map<String, BigInteger> convertToMap(List<Object[]> data) {
        Map<String, BigInteger> resultMap = new HashMap<>();
        for (Object[] row : data) {
            String name = (String) row[0];
            BigInteger total = (BigInteger) row[1];
            resultMap.put(name, total);
        }
        return resultMap;
    }

}
