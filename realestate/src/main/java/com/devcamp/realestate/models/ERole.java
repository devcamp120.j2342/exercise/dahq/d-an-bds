package com.devcamp.realestate.models;

public enum ERole {
  ROLE_USER,
  ROLE_HOMESELLER,
  ROLE_ADMIN
}
