package com.devcamp.realestate.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.District;
import com.devcamp.realestate.entity.Province;
import com.devcamp.realestate.entity.Realestate;
import com.devcamp.realestate.entity.Ward;
import com.devcamp.realestate.models.User;
import com.devcamp.realestate.repository.DistrictRepository;
import com.devcamp.realestate.repository.ProvinceRepository;
import com.devcamp.realestate.repository.RealestateRepository;
import com.devcamp.realestate.repository.UserRepository;
import com.devcamp.realestate.repository.WardRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class RealestateController {
    @Autowired
    RealestateRepository realestateRepository;
    @Autowired
    UserRepository userRepository;

    @Autowired
    ProvinceRepository provinceRepository;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    WardRepository wardRepository;

    @GetMapping("/realestate")
    public List<Realestate> getAllRealestate() {
        return realestateRepository.findAll();
    }

    @PostMapping("/realestate")
    public ResponseEntity<Realestate> createRealestate(@RequestBody Realestate pRealestate) {
        try {
            return new ResponseEntity<>(realestateRepository.save(pRealestate), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/realestate/{id}/{idPro}/{idDis}/{idWard}")
    public ResponseEntity<Realestate> createRealById(@PathVariable("id") long id, @PathVariable("idPro") int idPro,
            @PathVariable("idDis") int idDis, @PathVariable("idWard") int idWard, @RequestBody Realestate pRealestate) {
        try {
            Optional<User> userData = userRepository.findById(id);
            Optional<Province> provinceData = provinceRepository.findById(idPro);
            Optional<District> districtData = districtRepository.findById(idDis);
            Optional<Ward> wardData = wardRepository.findById(idWard);
            Realestate realestateData = new Realestate();
            realestateData.setAcreage(pRealestate.getAcreage());
            realestateData.setAddress(pRealestate.getAddress());
            realestateData.setAdjacentAlleyMinWidth(pRealestate.getAdjacentAlleyMinWidth());
            realestateData.setAdjacentFacadeNum(pRealestate.getAdjacentFacadeNum());
            realestateData.setAdjacentRoad(pRealestate.getAdjacentRoad());
            realestateData.setAlleyMinWidth(pRealestate.getAlleyMinWidth());
            realestateData.setApartCode(pRealestate.getApartCode());
            realestateData.setApartLoca(pRealestate.getApartLoca());
            realestateData.setBalcony(pRealestate.getBalcony());
            realestateData.setBath(pRealestate.getBath());
            realestateData.setBedroom(pRealestate.getBedroom());
            realestateData.setCLCL(pRealestate.getCLCL());
            realestateData.setCTXDPrice(pRealestate.getCTXDPrice());
            realestateData.setCTXDValue(pRealestate.getCTXDValue());
            realestateData.setCreateBy(pRealestate.getCreateBy());
            realestateData.setDTSXD(pRealestate.getDTSXD());
            realestateData.setDateCreate(new Date());
            realestateData.setDescription(pRealestate.getDescription());
            realestateData.setDirection(pRealestate.getDirection());
            realestateData.setDistance2facade(pRealestate.getDistance2facade());
            realestateData.setFSBO(pRealestate.getFSBO());
            realestateData.setFactor(pRealestate.getFactor());
            realestateData.setFurnitureType(pRealestate.getFurnitureType());
            realestateData.setLandscapeView(pRealestate.getLandscapeView());
            realestateData.setLat(pRealestate.getLat());
            realestateData.setLegalDoc(pRealestate.getLegalDoc());
            realestateData.setLng(pRealestate.getLng());
            realestateData.setLongX(pRealestate.getLongX());
            realestateData.setNumberFloors(pRealestate.getNumberFloors());
            realestateData.setPartType(pRealestate.getPartType());
            realestateData.setPhoto(pRealestate.getPhoto());
            realestateData.setPrice(pRealestate.getPrice());
            realestateData.setPriceMin(pRealestate.getPriceMin());
            realestateData.setPriceRent(pRealestate.getPriceRent());
            realestateData.setPriceTime(pRealestate.getPriceTime());
            realestateData.setRequest(pRealestate.getRequest());
            realestateData.setReturnRate(pRealestate.getReturnRate());
            realestateData.setShape(pRealestate.getShape());
            realestateData.setStreetHouse(pRealestate.getStreetHouse());
            realestateData.setStructure(pRealestate.getStructure());
            realestateData.setTitle(pRealestate.getTitle());
            realestateData.setTotalFloors(pRealestate.getTotalFloors());
            realestateData.setType(pRealestate.getType());
            realestateData.setUpdateBy(pRealestate.getUpdateBy());
            realestateData.setViewNum(pRealestate.getViewNum());
            realestateData.setWallArea(pRealestate.getWallArea());
            realestateData.setWithY(pRealestate.getWithY());
            realestateData.setUser(userData.get());
            realestateData.setProvince(provinceData.get());
            realestateData.setDistrict(districtData.get());
            realestateData.setWard(wardData.get());
            return new ResponseEntity<>(realestateRepository.save(realestateData), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/realestate/{id}")
    public ResponseEntity<Realestate> updateRealestate(@PathVariable("id") int id,
            @RequestBody Realestate pRealestate) {
        try {
            Optional<Realestate> realestateData = realestateRepository.findById(id);
            if (realestateData.isPresent()) {
                realestateData.get().setAcreage(pRealestate.getAcreage());
                realestateData.get().setAddress(pRealestate.getAddress());
                realestateData.get().setAdjacentAlleyMinWidth(pRealestate.getAdjacentAlleyMinWidth());
                realestateData.get().setAdjacentFacadeNum(pRealestate.getAdjacentFacadeNum());
                realestateData.get().setAdjacentRoad(pRealestate.getAdjacentRoad());
                realestateData.get().setAlleyMinWidth(pRealestate.getAlleyMinWidth());
                realestateData.get().setApartCode(pRealestate.getApartCode());
                realestateData.get().setApartLoca(pRealestate.getApartLoca());
                realestateData.get().setBalcony(pRealestate.getBalcony());
                realestateData.get().setBath(pRealestate.getBath());
                realestateData.get().setBedroom(pRealestate.getBedroom());
                realestateData.get().setCLCL(pRealestate.getCLCL());
                realestateData.get().setCTXDPrice(pRealestate.getCTXDPrice());
                realestateData.get().setCTXDValue(pRealestate.getCTXDValue());
                realestateData.get().setCreateBy(pRealestate.getCreateBy());
                realestateData.get().setDTSXD(pRealestate.getDTSXD());
                realestateData.get().setDateCreate(new Date());
                realestateData.get().setDescription(pRealestate.getDescription());
                realestateData.get().setDirection(pRealestate.getDirection());
                realestateData.get().setDistance2facade(pRealestate.getDistance2facade());
                realestateData.get().setFSBO(pRealestate.getFSBO());
                realestateData.get().setFactor(pRealestate.getFactor());
                realestateData.get().setFurnitureType(pRealestate.getFurnitureType());
                realestateData.get().setLandscapeView(pRealestate.getLandscapeView());
                realestateData.get().setLat(pRealestate.getLat());
                realestateData.get().setLegalDoc(pRealestate.getLegalDoc());
                realestateData.get().setLng(pRealestate.getLng());
                realestateData.get().setLongX(pRealestate.getLongX());
                realestateData.get().setNumberFloors(pRealestate.getNumberFloors());
                realestateData.get().setPartType(pRealestate.getPartType());
                realestateData.get().setPhoto(pRealestate.getPhoto());
                realestateData.get().setPrice(pRealestate.getPrice());
                realestateData.get().setPriceMin(pRealestate.getPriceMin());
                realestateData.get().setPriceRent(pRealestate.getPriceRent());
                realestateData.get().setPriceTime(pRealestate.getPriceTime());
                realestateData.get().setRequest(pRealestate.getRequest());
                realestateData.get().setReturnRate(pRealestate.getReturnRate());
                realestateData.get().setShape(pRealestate.getShape());
                realestateData.get().setStreetHouse(pRealestate.getStreetHouse());
                realestateData.get().setStructure(pRealestate.getStructure());
                realestateData.get().setTitle(pRealestate.getTitle());
                realestateData.get().setTotalFloors(pRealestate.getTotalFloors());
                realestateData.get().setType(pRealestate.getType());
                realestateData.get().setUpdateBy(pRealestate.getUpdateBy());
                realestateData.get().setViewNum(pRealestate.getViewNum());
                realestateData.get().setWallArea(pRealestate.getWallArea());
                realestateData.get().setWithY(pRealestate.getWithY());
                realestateData.get().setStatus(pRealestate.getStatus());
                return new ResponseEntity<>(realestateRepository.save(realestateData.get()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/realestateUp/{id}")
    public ResponseEntity<Realestate> updateRealestateUp(@PathVariable("id") int id,
            @RequestBody Realestate pRealestate) {
        try {
            Optional<Realestate> realestateData = realestateRepository.findById(id);
            if (realestateData.isPresent()) {
                realestateData.get().setAcreage(pRealestate.getAcreage());
                realestateData.get().setAddress(pRealestate.getAddress());
                realestateData.get().setPartType(pRealestate.getPartType());

                realestateData.get().setBath(pRealestate.getBath());
                realestateData.get().setBedroom(pRealestate.getBedroom());

                realestateData.get().setDescription(pRealestate.getDescription());

                realestateData.get().setLongX(pRealestate.getLongX());

                realestateData.get().setPhoto(pRealestate.getPhoto());
                realestateData.get().setPrice(pRealestate.getPrice());

                realestateData.get().setRequest(pRealestate.getRequest());

                realestateData.get().setStructure(pRealestate.getStructure());
                realestateData.get().setTitle(pRealestate.getTitle());
                realestateData.get().setTotalFloors(pRealestate.getTotalFloors());

                realestateData.get().setWithY(pRealestate.getWithY());
                // realestateData.get().setStatus(pRealestate.getStatus());
                return new ResponseEntity<>(realestateRepository.save(realestateData.get()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/realestate/{id}")
    public ResponseEntity<Realestate> deleteRealestateById(@PathVariable("id") int id) {
        realestateRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/realestate/user/{id}")
    public ResponseEntity<Realestate> deleteRealByUserId(@PathVariable("id") int id) {
        realestateRepository.deleteRealUser(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/realestate/{id}")
    public Realestate getRealestateById(@PathVariable("id") int id) {
        return realestateRepository.findById(id).get();
    }

    @GetMapping("/realestateP")
    public ResponseEntity<List<Realestate>> getFiveReal(
            @RequestParam(value = "page", defaultValue = "") String page,
            @RequestParam(value = "size", defaultValue = "") String size) {
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<Realestate> list = new ArrayList<Realestate>();
            realestateRepository.findAll(pageWithFiveElements).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/filter/realestate/price/{pr1}/{pr2}")
    public ResponseEntity<List<Realestate>> paginationPrice(
            @RequestParam(value = "page", defaultValue = "") String page,
            @RequestParam(value = "size", defaultValue = "") String size,
            @PathVariable("pr1") int pr1, @PathVariable("pr2") int pr2) {
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<Realestate> list = new ArrayList<Realestate>();
            realestateRepository.fillterRealetateByPrice(pageWithFiveElements, pr1, pr2).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/filter/realestate/type/{pr1}")
    public List<Realestate> filterRealByType(@PathVariable("pr1") int pr1) {
        return realestateRepository.fillterRealetateByType(pr1);
    }

    @GetMapping("/filter/realestate/request/{pr1}")
    public List<Realestate> filterRealByRequest(@PathVariable("pr1") int pr1) {
        return realestateRepository.fillterRealetateByRequest(pr1);
    }

    // filter address
    @GetMapping("/filter/realestate/address/{pr1}/{pro2}/{pro3}")
    public ResponseEntity<List<Realestate>> paginationAddress(
            @RequestParam(value = "page", defaultValue = "") String page,
            @RequestParam(value = "size", defaultValue = "") String size,
            @PathVariable("pr1") int pr1, @PathVariable("pro2") int pro2, @PathVariable("pro3") int pro3) {
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<Realestate> list = new ArrayList<Realestate>();
            realestateRepository.fillterRealetateByAddress(pageWithFiveElements, pr1, pro2, pro3).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

    // pagin
    @GetMapping("/realestate-pagin")
    public ResponseEntity<List<Realestate>> pagination(
            @RequestParam(value = "page", defaultValue = "") String page,
            @RequestParam(value = "size", defaultValue = "") String size) {
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<Realestate> list = new ArrayList<Realestate>();
            realestateRepository.findAllReal(pageWithFiveElements).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

    // @GetMapping("/realestate/ranked")
    // public List<Realestate>

    @PutMapping("/realestate/approved/{id}")
    public ResponseEntity<Realestate> changeApproved(@PathVariable("id") int id) {
        try {
            Optional<Realestate> realData = realestateRepository.findById(id);
            realData.get().setStatus(1);
            return new ResponseEntity<>(realestateRepository.save(realData.get()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/realestate/pending/{id}")
    public ResponseEntity<Realestate> changePending(@PathVariable("id") int id) {
        try {
            Optional<Realestate> realData = realestateRepository.findById(id);
            realData.get().setStatus(0);
            return new ResponseEntity<>(realestateRepository.save(realData.get()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // demo pagin acreage
    @GetMapping("filter/realestate/acreage/{pr1}/{pr2}")
    public ResponseEntity<List<Realestate>> paginationAc(
            @RequestParam(value = "page", defaultValue = "") String page,
            @RequestParam(value = "size", defaultValue = "") String size,
            @PathVariable("pr1") int pr1, @PathVariable("pr2") int pr2) {
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<Realestate> list = new ArrayList<Realestate>();
            realestateRepository.fillterRealetateByAcreage(pageWithFiveElements, pr1, pr2).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

}
