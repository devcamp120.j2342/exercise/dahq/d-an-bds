package com.devcamp.realestate.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Location;
import com.devcamp.realestate.repository.LocationRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class LocationController {
    @Autowired
    LocationRepository locationRepository;

    @GetMapping("/location")
    public List<Location> getAllLocation() {
        return locationRepository.findAll();

    }

    @GetMapping("/location/{id}")
    public Location getLocationById(@PathVariable("id") int id) {
        return locationRepository.findById(id).get();
    }

    @PostMapping("/location")
    public ResponseEntity<Location> createLocation(@RequestBody Location pLocation) {
        try {
            return new ResponseEntity<>(locationRepository.save(pLocation), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/location/{id}")
    public ResponseEntity<Location> updateLocation(@PathVariable("id") int id, @RequestBody Location pLocation) {
        try {
            Optional<Location> locationData = locationRepository.findById(id);
            if (locationData.isPresent()) {
                locationData.get().setLatitude(pLocation.getLatitude());
                locationData.get().setLongitude(pLocation.getLongitude());
                return new ResponseEntity<>(locationRepository.save(locationData.get()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/location/{id}")
    public ResponseEntity<Location> deleteLocationById(@PathVariable("id") int id) {
        locationRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
