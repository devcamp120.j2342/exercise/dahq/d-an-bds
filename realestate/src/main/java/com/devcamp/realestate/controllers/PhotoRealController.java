package com.devcamp.realestate.controllers;

import java.util.List;
import java.util.Optional;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.PhotoReal;
import com.devcamp.realestate.entity.Realestate;
import com.devcamp.realestate.repository.PhotoRealRepository;
import com.devcamp.realestate.repository.RealestateRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class PhotoRealController {
    @Autowired
    PhotoRealRepository photoRealRepository;
    @Autowired
    RealestateRepository realestateRepository;

    @GetMapping("/photo")
    public List<PhotoReal> getAllPhoto() {
        return photoRealRepository.findAll();
    }

    @PostMapping("/photo/{id}")
    public ResponseEntity<PhotoReal> createPhoto(@PathVariable("id") int id, @RequestBody PhotoReal pPhotoReal) {
        try {
            PhotoReal photoData = new PhotoReal();
            Optional<Realestate> realData = realestateRepository.findById(id);
            photoData.setPhoto(pPhotoReal.getPhoto());
            photoData.setRealestate(realData.get());
            return new ResponseEntity<>(photoRealRepository.save(photoData), HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/photo/real/{id}")
    public ResponseEntity<PhotoReal> deletePhotoRealId(@PathVariable("id") int id) {
        photoRealRepository.deletePhotoByRealId(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/photo/demo/{id}")

    public ResponseEntity<PhotoReal> deletePhoto(@PathVariable("id") int id) {
        photoRealRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/photo/idreal")
    public List<PhotoReal> allLoadPhoto() {
        return photoRealRepository.allLoadPhoto();
    }

    @PutMapping("/photo/{idPhoto}/{idReal}")
    public ResponseEntity<PhotoReal> updatePhoto(@PathVariable("idPhoto") int idPhoto,
            @PathVariable("idReal") int idReal, @RequestBody PhotoReal photoReal) {
        try {
            Optional<PhotoReal> photoData = photoRealRepository.findById(idPhoto);
            Optional<Realestate> realData = realestateRepository.findById(idReal);
            photoData.get().setPhoto(photoReal.getPhoto());
            photoData.get().setRealestate(realData.get());
            return new ResponseEntity<>(photoRealRepository.save(photoData.get()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/photo/{id}")
    public ResponseEntity<PhotoReal> deletePhotoOrigin(@PathVariable("id") int id) {
        photoRealRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
