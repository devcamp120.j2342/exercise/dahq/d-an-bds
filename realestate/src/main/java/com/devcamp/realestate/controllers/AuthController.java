package com.devcamp.realestate.controllers;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.config.CustomRepositoryImplementationDetector;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Customers;
import com.devcamp.realestate.models.ERole;
import com.devcamp.realestate.models.Role;
import com.devcamp.realestate.models.User;
import com.devcamp.realestate.models.User1;
import com.devcamp.realestate.payload.request.LoginRequest;
import com.devcamp.realestate.payload.request.SignupRequest;
import com.devcamp.realestate.payload.response.JwtResponse;
import com.devcamp.realestate.payload.response.MessageResponse;
import com.devcamp.realestate.repository.CustomerRepository;
import com.devcamp.realestate.repository.RoleRepository;
import com.devcamp.realestate.repository.User1Repository;
import com.devcamp.realestate.repository.UserRepository;
import com.devcamp.realestate.security.jwt.JwtUtils;
import com.devcamp.realestate.security.jwt.JwtUtils1;
import com.devcamp.realestate.security.services.UserDetailsImpl;
import com.devcamp.realestate.security.services.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserRepository userRepository;

  @Autowired
  User1Repository user1Repository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  JwtUtils jwtUtils;

  @Autowired
  JwtUtils1 jwtUtils1;

  @Autowired
  UserService userService;

  @Autowired
  CustomerRepository customerRepository;

  @PostMapping("/signin")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);

    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());

    return ResponseEntity.ok(new JwtResponse(jwt,
        userDetails.getId(),
        userDetails.getUsername(),
        userDetails.getEmail(),
        roles));
  }

  @PostMapping("/signup")
  public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
    if (userRepository.existsByUsername(signUpRequest.getUsername())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Username is already taken!"));
    }

    if (userRepository.existsByEmail(signUpRequest.getEmail())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Email is already in use!"));
    }

    // Create new user's account
    User user = new User(signUpRequest.getUsername(),
        signUpRequest.getEmail(),
        encoder.encode(signUpRequest.getPassword()));

    Set<String> strRoles = signUpRequest.getRole();
    Set<Role> roles = new HashSet<>();

    if (strRoles == null) {
      Role userRole = roleRepository.findByName(ERole.ROLE_USER)
          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
      roles.add(userRole);
    } else {
      strRoles.forEach(role -> {
        switch (role) {
          case "admin":
            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(adminRole);

            break;
          case "employee":
            Role modRole = roleRepository.findByName(ERole.ROLE_HOMESELLER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(modRole);

            break;
          default:
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        }
      });
    }

    user.setRoles(roles);
    userRepository.save(user);

    return ResponseEntity.ok(new MessageResponse("" + user.getId()));

  }

  @PostMapping("/user/createAdmin")
  @PreAuthorize("hasRole('ADMIN')")

  public ResponseEntity<?> registerEmployee(@Valid @RequestBody SignupRequest signUpRequest) {

    if (userRepository.existsByUsername(signUpRequest.getUsername())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Username is already taken!"));
    }

    if (userRepository.existsByEmail(signUpRequest.getEmail())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Email is already in use!"));
    }

    // Create new user's account
    // Role roleData = roleRepository.findById((int) 2);

    User1 user1 = new User1(signUpRequest.getUsername(),
        signUpRequest.getEmail(),
        encoder.encode(signUpRequest.getPassword()));

    Set<String> strRoles = signUpRequest.getRole();
    Set<Role> roles = new HashSet<>();

    if (strRoles == null) {
      Role userRole = roleRepository.findByName(ERole.ROLE_HOMESELLER)
          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
      roles.add(userRole);
    } else {
      strRoles.forEach(role -> {
        switch (role) {
          case "admin":
            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(adminRole);

            break;
          case "employee":
            Role modRole = roleRepository.findByName(ERole.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(modRole);

            break;
          default:
            Role userRole = roleRepository.findByName(ERole.ROLE_HOMESELLER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        }
      });
    }
    // roles.add(roleData);
    user1.setRoles(roles);
    user1Repository.save(user1);

    return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
  }

  // add customer khi register

  // @PostMapping("/customer/{idUser}")
  // public ResponseEntity<Customers> createCustomer(
  // @PathVariable("idUser") long idUser) {
  // try {
  // Optional<User> userData = userRepository.findById(idUser);
  // Customers customerData = new Customers();
  // customerData.setAddress("");
  // customerData.setContactName(userData.get().getUsername());
  // customerData.setContactTitle("");
  // customerData.setCreateBy("");
  // customerData.setCreateDate(new Date());

  // customerData.setMobile("");
  // customerData.setNote("");
  // customerData.setUpdateBy("");
  // customerData.setUpdateDate(new Date());
  // customerData.setUser(userData.get());

  // return new ResponseEntity<>(customerRepository.save(customerData),
  // HttpStatus.CREATED);
  // } catch (Exception e) {
  // return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  // }

  // }
  @PostMapping("/register/customer/{id}")
  public ResponseEntity<Customers> createCustomer(@PathVariable("id") long id,
      @RequestBody Customers pCustomers) {
    try {
      Optional<User> userData = userRepository.findById(id);
      Customers customerData = new Customers();
      customerData.setAddress(pCustomers.getAddress());
      customerData.setContactName(userData.get().getUsername());
      customerData.setCreateBy(pCustomers.getCreateBy());
      customerData.setCreateDate(new Date());
      customerData.setMobile(pCustomers.getMobile());
      customerData.setEmail(userData.get().getEmail());
      customerData.setNote(pCustomers.getNote());
      customerData.setUpdateBy(pCustomers.getUpdateBy());
      customerData.setUpdateDate(new Date());
      customerData.setUser(userData.get());

      return new ResponseEntity<>(customerRepository.save(customerData),
          HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  @GetMapping("/abc")
  public String hello() {
    return "hello";
  }

  @DeleteMapping("/role/user/{id}")
  public void deleteRoleUser(@PathVariable("id") long id) {
    roleRepository.deleteRoleUser(id);
  }

}
