package com.devcamp.realestate.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Project;
import com.devcamp.realestate.repository.ProjectRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ProjectController {
    @Autowired
    ProjectRepository projectRepository;

    @GetMapping("/project")
    public List<Project> getAllProject() {
        return projectRepository.findAll();
    }

}
