package com.devcamp.realestate.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.AddressMap;
import com.devcamp.realestate.repository.AddressMapRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class AddressMapController {
    @Autowired
    AddressMapRepository addressMapRepository;

    @GetMapping("/addressMap")
    public List<AddressMap> getAllAddressMap() {
        return addressMapRepository.findAll();
    }

    @GetMapping("/addressMap/{id}")
    public AddressMap getAddressMapById(@PathVariable("id") int id) {
        return addressMapRepository.findById(id).get();
    }

    @PostMapping("/addressMap")
    public ResponseEntity<AddressMap> createAddressMap(@RequestBody AddressMap pAddressMap) {
        try {
            return new ResponseEntity<>(addressMapRepository.save(pAddressMap), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/addressMap/{id}")
    public ResponseEntity<AddressMap> updateAddressMap(@PathVariable("id") int id,
            @RequestBody AddressMap pAddressMap) {
        try {
            Optional<AddressMap> addressData = addressMapRepository.findById(id);
            if (addressData.isPresent()) {
                addressData.get().setAddress(pAddressMap.getAddress());
                addressData.get().setLat(pAddressMap.getLat());
                addressData.get().setLng(pAddressMap.getLng());
                return new ResponseEntity<>(addressMapRepository.save(addressData.get()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/addressMap/{id}")
    public ResponseEntity<AddressMap> deleteAddressMap(@PathVariable("id") int id) {
        addressMapRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
