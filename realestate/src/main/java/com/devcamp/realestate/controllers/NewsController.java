package com.devcamp.realestate.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.News;
import com.devcamp.realestate.repository.NewsRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class NewsController {
    @Autowired
    NewsRepository newsRepository;

    @GetMapping("/news")
    public List<News> getAllNews() {
        return newsRepository.findAll();
    }

    @PostMapping("/news")
    public ResponseEntity<News> createNews(@RequestBody News pNews) {
        try {
            News newData = new News();
            newData.setAuthor(pNews.getAuthor());
            newData.setContent(pNews.getContent());

            newData.setCreateDate(new Date());
            newData.setUpdateDate(new Date());
            newData.setPhoto(pNews.getPhoto());
            newData.setTopic(pNews.getTopic());
            newData.setTitle(pNews.getTitle());
            return new ResponseEntity<>(newsRepository.save(newData), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/news/{id}")
    public ResponseEntity<News> updateNews(@PathVariable("id") int id, @RequestBody News pNews) {
        try {
            Optional<News> newsData = newsRepository.findById(id);
            newsData.get().setAuthor(pNews.getAuthor());
            newsData.get().setContent(pNews.getContent());
            newsData.get().setPhoto(pNews.getPhoto());
            newsData.get().setTopic(pNews.getTopic());
            newsData.get().setUpdateDate(new Date());
            newsData.get().setTitle(pNews.getTitle());
            return new ResponseEntity<>(newsRepository.save(newsData.get()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/news/{id}")
    public ResponseEntity<News> deleteNews(@PathVariable("id") int id) {
        newsRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("newslasted")
    public News getNewsLasted() {
        return newsRepository.findIdNewsLasted().get();
    }

}
