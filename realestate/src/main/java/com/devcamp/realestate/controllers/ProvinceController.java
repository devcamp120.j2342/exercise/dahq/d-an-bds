package com.devcamp.realestate.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Province;
import com.devcamp.realestate.repository.ProvinceRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ProvinceController {
    @Autowired
    ProvinceRepository provinceRepository;

    @GetMapping("/province")
    public List<Province> getAllProvince() {
        return provinceRepository.findAll();
    }

    @GetMapping("/province/{id}")
    public Province getProvinceById(@PathVariable("id") int id) {
        return provinceRepository.findById(id).get();
    }

}
