package com.devcamp.realestate.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.swing.text.html.Option;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Customers;
import com.devcamp.realestate.entity.Employees;
import com.devcamp.realestate.models.User;
import com.devcamp.realestate.repository.CustomerRepository;
import com.devcamp.realestate.repository.UserRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CustomerController {
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    UserRepository userRepository;

    @GetMapping("/customer")
    public List<Customers> getAllCustomers() {
        return customerRepository.findAll();

    }

    @GetMapping("/customer/{id}")
    public Customers getCustomer(@PathVariable("id") int id) {
        return customerRepository.findById(id).get();
    }

    @PostMapping("/Customer/{id}")
    public ResponseEntity<Customers> createCustomer(@PathVariable("id") long id,
            @RequestBody Customers pCustomers) {
        try {
            Optional<User> userData = userRepository.findById(id);
            Customers customerData = new Customers();
            customerData.setAddress(pCustomers.getAddress());
            customerData.setContactName(userData.get().getUsername());
            customerData.setCreateBy(pCustomers.getCreateBy());
            customerData.setCreateDate(new Date());
            customerData.setMobile(pCustomers.getMobile());
            customerData.setNote(pCustomers.getNote());
            customerData.setUpdateBy(pCustomers.getUpdateBy());
            customerData.setUpdateDate(new Date());
            customerData.setUser(userData.get());

            return new ResponseEntity<>(customerRepository.save(customerData),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/customer/{id}")
    public ResponseEntity<Customers> updateCustomer(@RequestBody Customers pCustomers, @PathVariable("id") int id) {
        try {
            Optional<Customers> customerData = customerRepository.findById(id);
            if (customerData.isPresent()) {
                customerData.get().setAddress(pCustomers.getAddress());
                customerData.get().setContactName(pCustomers.getContactName());
                customerData.get().setContactTitle(pCustomers.getContactTitle());
                // customerData.get().setCreateBy(pCustomers.getCreateBy());
                // customerData.get().setCreateDate(new Date());

                customerData.get().setMobile(pCustomers.getMobile());
                customerData.get().setNote(pCustomers.getNote());
                // customerData.get().setUpdateBy(pCustomers.getUpdateBy());
                customerData.get().setUpdateDate(new Date());

                customerData.get().setEmail(pCustomers.getEmail());
                customerData.get().setDescription(pCustomers.getDescription());
                return new ResponseEntity<>(customerRepository.save(customerData.get()), HttpStatus.OK);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/customer/updateAvata/{id}")
    public ResponseEntity<Customers> updateCustomerAtata(@PathVariable("id") int id,
            @RequestBody Customers pCustomers) {
        try {
            Optional<Customers> customerData = customerRepository.findById(id);
            customerData.get().setAvata(pCustomers.getAvata());
            return new ResponseEntity<>(customerRepository.save(customerData.get()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/customer/{id}")
    public ResponseEntity<Customers> deleteCustomer(@PathVariable("id") int id) {
        customerRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
