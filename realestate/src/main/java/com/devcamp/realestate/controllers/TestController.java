package com.devcamp.realestate.controllers;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.models.User;
import com.devcamp.realestate.repository.UserRepository;
import com.devcamp.realestate.security.services.UserDetailsServiceImpl;
import com.devcamp.realestate.security.services.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/users")
public class TestController {
  @Autowired
  UserDetailsServiceImpl userDetailsServiceImpl;

  @Autowired
  UserRepository userRepository;

  @Autowired
  UserService userService;

  @GetMapping("/all")
  public String allAccess() {
    return "Public Content.";
  }

  @GetMapping("/me")
  @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
  public User userAccess(HttpServletRequest req) {
    return userDetailsServiceImpl.whoami(req);
  }

  @GetMapping("/mod")
  @PreAuthorize("hasRole('MODERATOR')")
  public String moderatorAccess() {
    return "Moderator Board.";
  }

  @GetMapping("/admin")
  @PreAuthorize("hasRole('ADMIN')")
  public String adminAccess() {
    return "Admin Board.";
  }

  @GetMapping("/allUser")
  public List<User> getAllUser() {
    return userRepository.findAll();
  }

  @GetMapping("/userReal/{id}")
  public User getUserByReal(@PathVariable("id") int id) {
    return userRepository.findIdUserByIdRealetate(id).get();
  }

  @GetMapping("/user/{id}")
  public User getUserById(@PathVariable("id") long id) {
    return userRepository.findById(id).get();
  }

  @GetMapping("/user/total")
  public Map<String, BigInteger> getCustomerTotal() {
    return userService.getUserTotal();
  }
}
