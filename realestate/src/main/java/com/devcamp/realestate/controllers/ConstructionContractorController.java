package com.devcamp.realestate.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.ConstructionContractor;
import com.devcamp.realestate.repository.ConstructionContractorRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ConstructionContractorController {
    @Autowired
    ConstructionContractorRepository constructionContractorRepository;

    @GetMapping("/construction")
    public List<ConstructionContractor> getAllConstruction() {
        return constructionContractorRepository.findAll();
    }

    @GetMapping("/construction/{id}")
    public ConstructionContractor getConstructionById(@PathVariable("id") int id) {
        return constructionContractorRepository.findById(id).get();
    }

    @PostMapping("/construction")
    public ResponseEntity<ConstructionContractor> createConstruction(
            @RequestBody ConstructionContractor pConstructionContractor) {
        try {
            return new ResponseEntity<>(constructionContractorRepository.save(pConstructionContractor),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/construction/{id}")
    public ResponseEntity<ConstructionContractor> updateConstruction(@PathVariable("id") int id,
            @RequestBody ConstructionContractor pConstructionContractor) {
        try {
            Optional<ConstructionContractor> constructionData = constructionContractorRepository.findById(id);
            if (constructionData.isPresent()) {
                constructionData.get().setAddress(pConstructionContractor.getAddress());
                constructionData.get().setDescription(pConstructionContractor.getDescription());
                constructionData.get().setEmail(pConstructionContractor.getEmail());
                constructionData.get().setFax(pConstructionContractor.getFax());
                constructionData.get().setName(pConstructionContractor.getName());
                constructionData.get().setNote(pConstructionContractor.getNote());
                constructionData.get().setPhone(pConstructionContractor.getPhone());
                constructionData.get().setPhone2(pConstructionContractor.getPhone2());
                // constructionData.get().setProject(pConstructionContractor.getProject());
                constructionData.get().setWebsite(pConstructionContractor.getWebsite());
                return new ResponseEntity<>(constructionContractorRepository.save(constructionData.get()),
                        HttpStatus.CREATED);

            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }

    @DeleteMapping("/construction/{id}")
    public ResponseEntity<ConstructionContractor> deleteConstruction(@PathVariable("id") int id) {
        constructionContractorRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
