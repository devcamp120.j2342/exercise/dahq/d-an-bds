package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.Investor;

public interface InvestorRepository extends JpaRepository<Investor, Integer> {

}
