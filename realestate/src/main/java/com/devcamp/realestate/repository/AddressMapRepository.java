package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.AddressMap;

public interface AddressMapRepository extends JpaRepository<AddressMap, Integer> {

}
