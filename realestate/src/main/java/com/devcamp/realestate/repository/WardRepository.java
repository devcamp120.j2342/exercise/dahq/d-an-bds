package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.Ward;

public interface WardRepository extends JpaRepository<Ward, Integer> {

}
