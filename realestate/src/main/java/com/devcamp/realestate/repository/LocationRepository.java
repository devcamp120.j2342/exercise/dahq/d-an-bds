package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.Location;

public interface LocationRepository extends JpaRepository<Location, Integer> {

}
