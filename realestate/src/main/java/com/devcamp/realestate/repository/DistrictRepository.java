package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.District;

public interface DistrictRepository extends JpaRepository<District, Integer> {

}
