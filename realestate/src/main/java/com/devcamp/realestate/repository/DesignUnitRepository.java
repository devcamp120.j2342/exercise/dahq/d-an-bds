package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.DesignUnit;

public interface DesignUnitRepository extends JpaRepository<DesignUnit, Integer> {

}
