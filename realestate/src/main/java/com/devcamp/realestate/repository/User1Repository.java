package com.devcamp.realestate.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.models.User;
import com.devcamp.realestate.models.User1;

public interface User1Repository extends JpaRepository<User1, Long> {
    Optional<User1> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    User1 save(User1 user);
}
