package com.devcamp.realestate.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.realestate.entity.PhotoReal;
import com.devcamp.realestate.entity.Realestate;

public interface PhotoRealRepository extends JpaRepository<PhotoReal, Integer> {
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM photo_real WHERE realestate_id=:a", nativeQuery = true)
    void deletePhotoByRealId(@Param("a") int a);

    @Query(value = "SELECT pr FROM photo_real pr JOIN FETCH pr.realestate", nativeQuery = true)
    List<PhotoReal> allLoadPhoto();

}
