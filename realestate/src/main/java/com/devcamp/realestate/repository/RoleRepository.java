package com.devcamp.realestate.repository;

import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.realestate.entity.Realestate;
import com.devcamp.realestate.models.ERole;
import com.devcamp.realestate.models.Role;
import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {
  Optional<Role> findByName(ERole name);

  Role findById(int id);

  @Query(value = " SELECT * FROM user_roles WHERE user_id=:a ", nativeQuery = true)
  void deleteRoleUser(@Param("a") long a);

}
