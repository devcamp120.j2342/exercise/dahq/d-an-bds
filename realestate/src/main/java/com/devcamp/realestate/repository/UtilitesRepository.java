package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.Utilities;

public interface UtilitesRepository extends JpaRepository<Utilities, Integer> {

}
