package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.ConstructionContractor;

public interface ConstructionContractorRepository extends JpaRepository<ConstructionContractor, Integer> {

}
