package com.devcamp.realestate.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.realestate.entity.Realestate;

public interface RealestateRepository extends JpaRepository<Realestate, Integer> {
    @Query(value = "SELECT * FROM realestate WHERE price >= :a1 AND price <= :a2", nativeQuery = true)
    List<Realestate> fillterRealetateByPrice(Pageable pageable, @Param("a1") int a1, @Param("a2") int a2);

    @Query(value = "SELECT * FROM realestate WHERE acreage >= :a1 AND acreage <= :a2", nativeQuery = true)
    List<Realestate> fillterRealetateByAcreage(Pageable pageable, @Param("a1") int a1, @Param("a2") int a2);

    @Query(value = "SELECT * FROM realestate WHERE apart_type=:a", nativeQuery = true)
    List<Realestate> fillterRealetateByType(@Param("a") int a);

    @Query(value = "SELECT * FROM realestate WHERE request=:a", nativeQuery = true)
    List<Realestate> fillterRealetateByRequest(@Param("a") int a);

    @Query(value = "SELECT * FROM `realestate` WHERE province_id=:a1 OR district_id=:a2 OR wards_id=:a3", nativeQuery = true)
    List<Realestate> fillterRealetateByAddress(Pageable pageable, @Param("a1") int a1, @Param("a2") int a2,
            @Param("a3") int a3);

    @Query(value = " SELECT * FROM realestate WHERE status=1 ORDER BY date_create DESC", nativeQuery = true)
    List<Realestate> findAllReal(Pageable pageable);

    @Modifying
    @Transactional

    @Query(value = " SELECT * FROM realestate WHERE user_id=:a", nativeQuery = true)
    void deleteRealUser(@Param("a") int a);

}
