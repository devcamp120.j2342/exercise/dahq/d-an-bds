package com.devcamp.realestate.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.realestate.entity.News;

public interface NewsRepository extends JpaRepository<News, Integer> {
    @Query(value = "SELECT * FROM news WHERE create_date = (SELECT MAX(create_date) FROM news)", nativeQuery = true)
    Optional<News> findIdNewsLasted();
}
