package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.Customers;

public interface CustomerRepository extends JpaRepository<Customers, Integer> {

}
