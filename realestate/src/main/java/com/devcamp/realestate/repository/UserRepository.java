package com.devcamp.realestate.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.realestate.models.User;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
  Optional<User> findByUsername(String username);

  Boolean existsByUsername(String username);

  Boolean existsByEmail(String email);

  Optional<User> findById(Long id);

  User save(User user);

  @Query(value = "SELECT users.* FROM users INNER JOIN realestate ON users.id = realestate.user_id WHERE realestate.id = ?1", nativeQuery = true)
  Optional<User> findIdUserByIdRealetate(int realestateId);

  @Query(value = "SELECT u.username AS username, COUNT(r.id) AS real_estate_count FROM users u LEFT JOIN realestate r ON u.id = r.user_id WHERE u.id = user_id GROUP BY u.id", nativeQuery = true)
  List<Object[]> getUserTotal();
}
