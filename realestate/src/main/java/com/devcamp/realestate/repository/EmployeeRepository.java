package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.Employees;

public interface EmployeeRepository extends JpaRepository<Employees, Integer> {

}
