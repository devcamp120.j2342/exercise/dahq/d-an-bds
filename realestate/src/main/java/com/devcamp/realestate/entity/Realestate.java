package com.devcamp.realestate.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.devcamp.realestate.models.User;

@Entity
@Table(name = "realestate")
public class Realestate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    private int type;
    private int request;
    @ManyToOne
    @JoinColumn(name = "province_id")
    private Province province;

    @ManyToOne
    @JoinColumn(name = "district_id")
    private District district;
    @ManyToOne
    @JoinColumn(name = "wards_id")
    private Ward ward;
    @ManyToOne
    @JoinColumn(name = "street_id")
    private Street street;
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    private String address;

    private long price;
    @Column(name = "price_min")
    private long priceMin;
    @Column(name = "price_time")
    private int priceTime;
    @Column(name = "date_create")
    private Date dateCreate;

    private double acreage;
    private int direction;
    @Column(name = "total_floors")
    private int totalFloors;
    @Column(name = "number_floors")
    private int numberFloors;
    private int bath;
    @Column(name = "apart_code")
    private String apartCode;
    @Column(name = "wall_area")
    private double wallArea;

    private int bedroom;

    private int balcony;
    @Column(name = "landscape_view")
    private String landscapeView;
    @Column(name = "apart_loca")
    private int apartLoca;
    @Column(name = "apart_type")
    private int partType;
    @Column(name = "furniture_type")
    private int furnitureType;
    @Column(name = "price_rent")
    private int priceRent;
    @Column(name = "return_rate")
    private double returnRate;
    @Column(name = "legal_doc")
    private int legalDoc;

    private String description;
    @Column(name = "width_y")
    private int withY;
    @Column(name = "long_x")
    private int longX;
    @Column(name = "street_house")
    private int streetHouse;
    private int FSBO;
    @Column(name = "view_num")
    private int viewNum;
    @Column(name = "create_by")
    private int createBy;
    @Column(name = "update_by")
    private int updateBy;
    private String shape;

    private int distance2facade;
    @Column(name = "adjacent_facade_num")
    private int adjacentFacadeNum;
    @Column(name = "adjacent_road")
    private String adjacentRoad;
    @Column(name = "alley_min_width")
    private int alleyMinWidth;
    @Column(name = "adjacent_alley_min_width")
    private int adjacentAlleyMinWidth;
    private int factor;
    private String structure;
    private int DTSXD;
    private int CLCL;
    @Column(name = "CTXD_price")
    private int CTXDPrice;
    @Column(name = "CTXD_value")
    private int CTXDValue;
    private String photo;
    @Column(name = "_lat")
    private double lat;
    @Column(name = "_lng")
    private double lng;

    // authorization
    private int status = 0;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "realestate")

    private List<PhotoReal> photoReals;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getRequest() {
        return request;
    }

    public void setRequest(int request) {
        this.request = request;
    }

    // public Province getProvince() {
    // return province;
    // }

    public void setProvince(Province province) {
        this.province = province;
    }

    // public District getDistrict() {
    // return district;
    // }

    public void setDistrict(District district) {
        this.district = district;
    }

    // public Ward getWard() {
    // return ward;
    // }

    public void setWard(Ward ward) {
        this.ward = ward;
    }

    // public Street getStreet() {
    // return street;
    // }

    public void setStreet(Street street) {
        this.street = street;
    }

    // public Project getProject() {
    // return project;
    // }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(long priceMin) {
        this.priceMin = priceMin;
    }

    public int getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(int priceTime) {
        this.priceTime = priceTime;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public double getAcreage() {
        return acreage;
    }

    public void setAcreage(double acreage) {
        this.acreage = acreage;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getTotalFloors() {
        return totalFloors;
    }

    public void setTotalFloors(int totalFloors) {
        this.totalFloors = totalFloors;
    }

    public int getNumberFloors() {
        return numberFloors;
    }

    public void setNumberFloors(int numberFloors) {
        this.numberFloors = numberFloors;
    }

    public int getBath() {
        return bath;
    }

    public void setBath(int bath) {
        this.bath = bath;
    }

    public String getApartCode() {
        return apartCode;
    }

    public void setApartCode(String apartCode) {
        this.apartCode = apartCode;
    }

    public double getWallArea() {
        return wallArea;
    }

    public void setWallArea(double wallArea) {
        this.wallArea = wallArea;
    }

    public int getBedroom() {
        return bedroom;
    }

    public void setBedroom(int bedroom) {
        this.bedroom = bedroom;
    }

    public int getBalcony() {
        return balcony;
    }

    public void setBalcony(int balcony) {
        this.balcony = balcony;
    }

    public String getLandscapeView() {
        return landscapeView;
    }

    public void setLandscapeView(String landscapeView) {
        this.landscapeView = landscapeView;
    }

    public int getApartLoca() {
        return apartLoca;
    }

    public void setApartLoca(int apartLoca) {
        this.apartLoca = apartLoca;
    }

    public int getPartType() {
        return partType;
    }

    public void setPartType(int partType) {
        this.partType = partType;
    }

    public int getFurnitureType() {
        return furnitureType;
    }

    public void setFurnitureType(int furnitureType) {
        this.furnitureType = furnitureType;
    }

    public int getPriceRent() {
        return priceRent;
    }

    public void setPriceRent(int priceRent) {
        this.priceRent = priceRent;
    }

    public double getReturnRate() {
        return returnRate;
    }

    public void setReturnRate(double returnRate) {
        this.returnRate = returnRate;
    }

    public int getLegalDoc() {
        return legalDoc;
    }

    public void setLegalDoc(int legalDoc) {
        this.legalDoc = legalDoc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWithY() {
        return withY;
    }

    public void setWithY(int withY) {
        this.withY = withY;
    }

    public int getLongX() {
        return longX;
    }

    public void setLongX(int longX) {
        this.longX = longX;
    }

    public int getStreetHouse() {
        return streetHouse;
    }

    public void setStreetHouse(int streetHouse) {
        this.streetHouse = streetHouse;
    }

    public int getFSBO() {
        return FSBO;
    }

    public void setFSBO(int fSBO) {
        FSBO = fSBO;
    }

    public int getViewNum() {
        return viewNum;
    }

    public void setViewNum(int viewNum) {
        this.viewNum = viewNum;
    }

    public int getCreateBy() {
        return createBy;
    }

    public void setCreateBy(int createBy) {
        this.createBy = createBy;
    }

    public int getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(int updateBy) {
        this.updateBy = updateBy;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public int getDistance2facade() {
        return distance2facade;
    }

    public void setDistance2facade(int distance2facade) {
        this.distance2facade = distance2facade;
    }

    public int getAdjacentFacadeNum() {
        return adjacentFacadeNum;
    }

    public void setAdjacentFacadeNum(int adjacentFacadeNum) {
        this.adjacentFacadeNum = adjacentFacadeNum;
    }

    public String getAdjacentRoad() {
        return adjacentRoad;
    }

    public void setAdjacentRoad(String adjacentRoad) {
        this.adjacentRoad = adjacentRoad;
    }

    public int getAlleyMinWidth() {
        return alleyMinWidth;
    }

    public void setAlleyMinWidth(int alleyMinWidth) {
        this.alleyMinWidth = alleyMinWidth;
    }

    public int getAdjacentAlleyMinWidth() {
        return adjacentAlleyMinWidth;
    }

    public void setAdjacentAlleyMinWidth(int adjacentAlleyMinWidth) {
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public int getDTSXD() {
        return DTSXD;
    }

    public void setDTSXD(int dTSXD) {
        DTSXD = dTSXD;
    }

    public int getCLCL() {
        return CLCL;
    }

    public void setCLCL(int cLCL) {
        CLCL = cLCL;
    }

    public int getCTXDPrice() {
        return CTXDPrice;
    }

    public void setCTXDPrice(int cTXDPrice) {
        CTXDPrice = cTXDPrice;
    }

    public int getCTXDValue() {
        return CTXDValue;
    }

    public void setCTXDValue(int cTXDValue) {
        CTXDValue = cTXDValue;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Realestate(int id, String title, int type, int request, Province province, District district, Ward ward,
            Street street, Project project, String address, Customers customers, long price, long priceMin,
            int priceTime, Date dateCreate, double acreage, int direction, int totalFloors, int numberFloors, int bath,
            String apartCode, double wallArea, int bedroom, int balcony, String landscapeView, int apartLoca,
            int partType, int furnitureType, int priceRent, double returnRate, int legalDoc, String description,
            int withY, int longX, int streetHouse, int fSBO, int viewNum, int createBy, int updateBy, String shape,
            int distance2facade, int adjacentFacadeNum, String adjacentRoad, int alleyMinWidth,
            int adjacentAlleyMinWidth, int factor, String structure, int dTSXD, int cLCL, int cTXDPrice, int cTXDValue,
            String photo, double lat, double lng, int status) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.request = request;
        this.province = province;
        this.district = district;
        this.ward = ward;
        this.street = street;

        this.address = address;
        // this.cuss = customers;
        this.price = price;
        this.priceMin = priceMin;
        this.priceTime = priceTime;
        this.dateCreate = dateCreate;
        this.acreage = acreage;
        this.direction = direction;
        this.totalFloors = totalFloors;
        this.numberFloors = numberFloors;
        this.bath = bath;
        this.apartCode = apartCode;
        this.wallArea = wallArea;
        this.bedroom = bedroom;
        this.balcony = balcony;
        this.landscapeView = landscapeView;
        this.apartLoca = apartLoca;
        this.partType = partType;
        this.furnitureType = furnitureType;
        this.priceRent = priceRent;
        this.returnRate = returnRate;
        this.legalDoc = legalDoc;
        this.description = description;
        this.withY = withY;
        this.longX = longX;
        this.streetHouse = streetHouse;
        FSBO = fSBO;
        this.viewNum = viewNum;
        this.createBy = createBy;
        this.updateBy = updateBy;
        this.shape = shape;
        this.distance2facade = distance2facade;
        this.adjacentFacadeNum = adjacentFacadeNum;
        this.adjacentRoad = adjacentRoad;
        this.alleyMinWidth = alleyMinWidth;
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
        this.factor = factor;
        this.structure = structure;
        DTSXD = dTSXD;
        CLCL = cLCL;
        CTXDPrice = cTXDPrice;
        CTXDValue = cTXDValue;
        this.photo = photo;
        this.lat = lat;
        this.lng = lng;
        this.status = status;
    }

    public Realestate() {
    }

    // public User getUser() {
    // return user;
    // }

    public void setUser(User user) {
        this.user = user;
    }

    public List<PhotoReal> getPhotoReals() {
        return photoReals;
    }

    public void setPhotoReals(List<PhotoReal> photoReals) {
        this.photoReals = photoReals;
    }

}
