package com.devcamp.realestate.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "_name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "_province_id")
    private Province province;
    @ManyToOne
    @JoinColumn(name = "_district_id")
    private District district;
    @ManyToOne
    @JoinColumn(name = "_ward_id")
    private Ward ward;
    @ManyToOne
    @JoinColumn(name = "_street_id")
    private Street street;

    private String address;
    private String slogan;
    private String description;
    private double acreage;
    @Column(name = "construct_area")
    private double constructArea;
    @Column(name = "num_block")
    private int numBlock;
    @Column(name = "num_floors")
    private String numFloors;
    @Column(name = "num_apartment")
    private int numApartment;
    @Column(name = "apartmentt_area")
    private String apartmenttArea;
    // investor porject(1-n)
    @ManyToOne
    @JoinColumn(name = "investor_id")
    private Investor investors;

    // Design unit
    @ManyToOne
    @JoinColumn(name = "design_unit_id")
    private DesignUnit designUnits;

    // construction contractor
    @ManyToOne
    @JoinColumn(name = "contractor_id")
    private ConstructionContractor constructionContractors;

    // utilities
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "project_utilities", joinColumns = @JoinColumn(name = "project_id"), inverseJoinColumns = @JoinColumn(name = "utilities_id"))
    private List<Utilities> utilities;

    @Column(name = "construction_contractor")
    private int constructionContractor;

    @Column(name = "region_link")
    private String regionLink;
    private String photo;
    @Column(name = "_lat")
    private double lat;
    @Column(name = "_lng")
    private double lng;
    @OneToMany(mappedBy = "project")
    private List<Realestate> realestates;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // public Province getProvince() {
    // return province;
    // }

    public void setProvince(Province province) {
        this.province = province;
    }

    // public District getDistrict() {
    // return district;
    // }

    public void setDistrict(District district) {
        this.district = district;
    }

    // public Ward getWard() {
    // return ward;
    // }

    public void setWard(Ward ward) {
        this.ward = ward;
    }

    // public Street getStreet() {
    // return street;
    // }

    public void setStreet(Street street) {
        this.street = street;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAcreage() {
        return acreage;
    }

    public void setAcreage(double acreage) {
        this.acreage = acreage;
    }

    public double getConstructArea() {
        return constructArea;
    }

    public void setConstructArea(double constructArea) {
        this.constructArea = constructArea;
    }

    public int getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(int numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloors() {
        return numFloors;
    }

    public void setNumFloors(String numFloors) {
        this.numFloors = numFloors;
    }

    public int getNumApartment() {
        return numApartment;
    }

    public void setNumApartment(int numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmenttArea() {
        return apartmenttArea;
    }

    public void setApartmenttArea(String apartmenttArea) {
        this.apartmenttArea = apartmenttArea;
    }

    public int getConstructionContractor() {
        return constructionContractor;
    }

    public void setConstructionContractor(int constructionContractor) {
        this.constructionContractor = constructionContractor;
    }

    public String getRegionLink() {
        return regionLink;
    }

    public void setRegionLink(String regionLink) {
        this.regionLink = regionLink;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public List<Realestate> getRealestates() {
        return realestates;
    }

    public void setRealestates(List<Realestate> realestates) {
        this.realestates = realestates;
    }

    public void setInvestors(Investor investors) {
        this.investors = investors;
    }

    public void setDesignUnits(DesignUnit designUnits) {
        this.designUnits = designUnits;
    }

    public void setConstructionContractors(ConstructionContractor constructionContractors) {
        this.constructionContractors = constructionContractors;
    }

    public List<Utilities> getUtilities() {
        return utilities;
    }

    // public void setUtilities(List<Utilities> utilities) {
    // this.utilities = utilities;
    // }

    public Project(int id, String name, Province province, District district, Ward ward, Street street, String address,
            String slogan, String description, double acreage, double constructArea, int numBlock, String numFloors,
            int numApartment, String apartmenttArea, int investor, int constructionContractor, int designUnit,
            String utilities, String regionLink, String photo, double lat, double lng, List<Realestate> realestates) {
        this.id = id;
        this.name = name;
        this.province = province;
        this.district = district;
        this.ward = ward;
        this.street = street;
        this.address = address;
        this.slogan = slogan;
        this.description = description;
        this.acreage = acreage;
        this.constructArea = constructArea;
        this.numBlock = numBlock;
        this.numFloors = numFloors;
        this.numApartment = numApartment;
        this.apartmenttArea = apartmenttArea;

        this.constructionContractor = constructionContractor;

        this.regionLink = regionLink;
        this.photo = photo;
        this.lat = lat;
        this.lng = lng;
        this.realestates = realestates;
    }

    public Project() {
    }

}
