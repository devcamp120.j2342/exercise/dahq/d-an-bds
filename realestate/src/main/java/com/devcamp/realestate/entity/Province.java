package com.devcamp.realestate.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "province")
public class Province {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "_name")
    private String name;
    @Column(name = "_code")
    private String code;
    @OneToMany(mappedBy = "province")
    private List<District> districts;
    @OneToMany(mappedBy = "province")
    private List<Street> streets;
    @OneToMany(mappedBy = "province")
    private List<Project> projects;
    @OneToMany(mappedBy = "province")
    private List<Realestate> realestates;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    public List<Street> getStreets() {
        return streets;
    }

    public void setStreets(List<Street> streets) {
        this.streets = streets;
    }

    public Province(int id, String name, String code, List<District> districts, List<Street> streets,
            List<Project> projects, List<Realestate> realestates) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.districts = districts;
        this.streets = streets;
        this.projects = projects;
        this.realestates = realestates;
    }

    public Province() {
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Realestate> getRealestates() {
        return realestates;
    }

    public void setRealestates(List<Realestate> realestates) {
        this.realestates = realestates;
    }

}
