package com.devcamp.realestate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "photo_real")
public class PhotoReal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String photo;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "realestate_id")
    private Realestate realestate;

    @Transient
    public int getRealestateId() {
        if (realestate != null) {
            return realestate.getId();
        }
        return 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public PhotoReal(int id, String photo) {
        this.id = id;
        this.photo = photo;
    }

    public PhotoReal() {
    }

    // public Realestate getRealestate() {
    // return realestate;
    // }

    public void setRealestate(Realestate realestate) {
        this.realestate = realestate;
    }

}
