
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/"
var urlParams = new URLSearchParams(window.location.search);
var accessToken = urlParams.get('access_token');

console.log(accessToken);
// Lưu giá trị user_id vào LocalStorage
// localStorage.setItem('token', accessToken);
// Lấy giá trị user_id từ LocalStorage
var accessToken = localStorage.getItem('token');
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onCLick();
    // loadProperties();
    callProperties();

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function callProperties() {
    $.ajax({
        url: gBASE_URL + "realestate",
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            console.log("success");

            loadProperties(paramRes);
        },
        error: function (paramRes) {
            alert(paramRes.status);
        }
    })
}
function loadProperties(paramData) {

    for (var bI = 0; bI < paramData.length; bI++) {
        if (paramData[bI].status == 1) {
            $("#row-realestate").append($("<div>", {
                class: "col-sm-4 mt-5",


            })
                .append($("<div>", {
                    class: "card card-properties",
                })
                    .append($("<img>", {
                        class: "card-img-top",
                        src: "./image/" + paramData[bI].photo,
                        height: "263.438"
                    }))
                    .append($("<div>", {
                        class: "card-body "

                    })
                        .append($("<p>", {
                            class: "title-properties",
                            html: paramData[bI].title,
                        }))
                        .append($("<p>", {
                            class: "address-properties",
                            html: paramData[bI].address,
                        }))
                        .append($("<div>", {
                            class: "row",
                        })
                            .append($("<div>", {
                                class: "col-sm-4",
                                html: '<img src="./image/area.png" alt="" class="img-icon-properties mr-1">' + paramData[bI].acreage + "m2"

                            }))
                            .append($("<div>", {
                                class: "col-sm-3",
                                html: '<img src="./image/bedroom.png" alt="" class="img-icon-properties mr-1">' + paramData[bI].bedroom

                            }))
                            .append($("<div>", {
                                class: "col-sm-3",
                                html: '<img src="./image/bathtub.png" alt="" class="img-icon-properties mr-1">' + paramData[bI].bath
                            }))
                            .append($("<div>", {
                                class: "col-sm-2",
                                html: '<img src="./image/tile.png" alt="" class="img-icon-properties mr-1">' + paramData[bI].totalFloors
                            }))
                        )
                    )
                    .append($("<div>", {
                        class: "card-footer",
                    })
                        .append($("<div>", {
                            class: "row mt-3",

                        })
                            .append($("<div>", {
                                class: "col-sm-6 price-properties",
                                html: "Price: $" + paramData[bI].price,
                            }))
                            .append($("<div>", {
                                class: "col-sm-6 text-right",
                                html: '<img src="./image/love-hand-drawn-heart-symbol-outline.png" alt="" class="img-icon-properties">'
                            }))
                        )
                    )
                )

            )
        }

    }

}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function onCLick() {
    $("#buy").on("click", function () {
        $("#buy").addClass("clicked");
    })

}


