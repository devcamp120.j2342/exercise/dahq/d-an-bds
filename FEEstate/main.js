
"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/"
// Lấy giá trị user_id từ LocalStorage
var accessToken = localStorage.getItem('token');
var vData = [];

// var vPage = 0;

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onCLick();

    callProperties(0);
    $("#all-price").on("click", function () {

        location.reload();

    })
    $("#under_1").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByPrice(1, 1, 0);
        onClickPagin("price", 1, 1);
    })
    $("#1-3").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByPrice(1, 3, 0);
        onClickPagin("price", 1, 3);
    })
    $("#3-5").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByPrice(3, 5, 0);
        onClickPagin("price", 3, 5);
    })
    $("#5-7").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByPrice(5, 7, 0);
        onClickPagin("price", 5, 7);
    })
    $("#7-10").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByPrice(7, 10, 0);
        onClickPagin("price", 7, 10);
    })
    $("#10-15").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByPrice(10, 15, 0);
        onClickPagin("price", 10, 15);
    })
    $("#15-20").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByPrice(15, 20, 0);
        onClickPagin("price", 15, 20);
    })
    $("#20-40").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByPrice(20, 40, 0);
        onClickPagin("price", 20, 40);
    })
    $("#40-50").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByPrice(40, 50, 0);
        onClickPagin("price", 40, 50);
    })
    $("#50-70").on("click", function () {

        $("#row-realestate").html("");
        onLoadFilterByPrice(50, 70, 0);
        onClickPagin("price", 50, 70);


    })
    $("#70-100").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByPrice(70, 100, 0);
        onClickPagin("price", 70, 100);
    })
    $("#over_100").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByPrice(100, 100000, 0);
        onClickPagin("price", 100, 100000);
    })

    $("#30-50m").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByAcreage(30, 50, 0);
        onClickPagin("ac", 30, 50);

    })
    $("#50-80m").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByAcreage(50, 80, 0);
        onClickPagin("ac", 50, 80);

    })
    $("#80-110m").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByAcreage(80, 110, 0);
        onClickPagin("ac", 80, 110);

    })
    $("#110-140m").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByAcreage(110, 140, 0);
        onClickPagin("ac", 110, 140);

    })
    $("#140-170m").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByAcreage(140, 170, 0);
        onClickPagin("ac", 140, 170);

    })
    $("#170-200m").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByAcreage(170, 200, 0);
        onClickPagin("ac", 170, 200);

    })
    $("#200-300m").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByAcreage(200, 300, 0);
        onClickPagin("ac", 200, 300);

    })
    $("#300-400m").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByAcreage(300, 400, 0);
        onClickPagin("ac", 300, 400);


    })
    $("#500-10000m").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByAcreage(500, 100000, 0);
        onClickPagin("ac", 500, 10000);

    })
    // filter type
    $("#btn-filter-type").on("click", function () {

        var vCheckboxed = $("input[type='checkbox'");
        var vSelectType = [];
        vCheckboxed.each(function () {
            if ($(this).prop("checked")) {
                vSelectType.push($(this).val());
            }
        })
        console.log(vSelectType);
        $("#row-realestate").html("");
        for (var bI = 0; bI < vSelectType.length; bI++) {
            onLoadFilterByType(vSelectType[bI]);
        }

    })
    $("#btn-reset-filter-type").on("click", function () {
        location.reload();
    })
    // filter request

    $("#filter-request-sale").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByRequest(1);
    })
    $("#filter-request-rent").on("click", function () {
        $("#row-realestate").html("");
        onLoadFilterByRequest(2);
    })


    // 


    // filter address
    // 
    $("#btn-search").on("click", function () {
        $("#row-realestate").html("");
        var vIdPro = $("#select-province").val();
        var vIdDis = $("#select-district").val();
        var vIdWard = $("#select-ward").val();
        onLoadFilterByAddress(vIdPro, vIdDis, vIdWard, 0);
        onClickPagin("address", vIdPro, vIdDis, vIdWard);

    })


    onLoadProvince();
    $("#select-province").on("change", function () {
        var vId = $("#select-province").val();
        $("#select-district").html("");
        onLoadDistrict(vId);
    })
    $("#select-district").on("change", function () {
        var vId = $("#select-district").val();
        $("#select-ward").html("");
        onLoadWard(vId);
    })
    // pagination
    $("#page1").on("click", function () {
        $("#row-realestate").html("");
        callProperties(0);


    })
    $("#page2").on("click", function () {
        $("#row-realestate").html("");
        callProperties(1);
    })
    $("#page3").on("click", function () {
        $("#row-realestate").html("");
        callProperties(2);
    })
    // and pagin

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function callProperties(paramPage) {
    $.ajax({
        url: gBASE_URL + "realestate-pagin?page=" + paramPage + "&size=6",

        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            console.log("success");
            console.log(paramRes);
            vData = paramRes;

            loadProperties(paramRes);
        },
        error: function (paramRes) {
            alert(paramRes.status);
        }
    })
}


function loadProperties(paramData) {

    for (var bI = 0; bI < paramData.length; bI++) {
        // if (paramData[bI].status == 0) {
        $("#row-realestate").append($("<div>", {
            class: "col-sm-4 mt-5",


        })
            .append($("<div>", {
                class: "card card-properties",
            })
                .append($("<a>", {
                    href: "DetailProperties.html?id=" + paramData[bI].id
                })
                    .append($("<img>", {
                        class: "card-img-top",
                        src: "./image/project/" + paramData[bI].photo,
                        // width: "280.5",
                        height: "201.656"

                    }))
                )

                .append($("<div>", {
                    class: "card-body "

                })
                    .append($("<p>", {
                        class: "title-properties",
                        html: paramData[bI].title,
                    }))
                    .append($("<p>", {
                        class: "address-properties",
                        html: paramData[bI].address,
                    }))
                    .append($("<div>", {
                        class: "row",
                    })
                        .append($("<div>", {
                            class: "col-sm-4",
                            html: '<img src="./image/area.png" alt="" class="img-icon-properties mr-1">' + paramData[bI].acreage + "m2"

                        }))
                        .append($("<div>", {
                            class: "col-sm-3",
                            html: '<img src="./image/bedroom.png" alt="" class="img-icon-properties mr-1">' + paramData[bI].bedroom

                        }))
                        .append($("<div>", {
                            class: "col-sm-3",
                            html: '<img src="./image/bathtub.png" alt="" class="img-icon-properties mr-1">' + paramData[bI].bath
                        }))
                        .append($("<div>", {
                            class: "col-sm-2",
                            html: '<img src="./image/tile.png" alt="" class="img-icon-properties mr-1">' + paramData[bI].totalFloors
                        }))
                    )
                )
                .append($("<div>", {
                    class: "card-footer",
                })
                    .append($("<div>", {
                        class: "row mt-3",

                    })
                        .append($("<div>", {
                            class: "col-sm-6 price-properties",
                            html: "Giá: " + paramData[bI].price + " Tỷ",
                        }))
                        .append($("<div>", {
                            class: "col-sm-6 text-right",
                            html: '<img src="./image/love-hand-drawn-heart-symbol-outline.png" alt="" class="img-icon-properties">'
                        }))
                    )
                )
            )

        )
    }

    // }

}




function findAcreage(paramAc1, paramAc2) {
    var vArrayResult = [];

    for (var bI = 0; bI < vData.length; bI++) {
        if (vData[bI].acreage >= paramAc1 && vData[bI].acreage <= paramAc2) {
            vArrayResult.push(vData[bI]);
        }

    }
    return vArrayResult;
}
function onLoadProvince() {
    $.ajax({
        url: gBASE_URL + "province",
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            handleProvince(paramRes);
            // console.log(accessToken)
            // alert("hello");
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onLoadDistrict(paramId) {
    $.ajax({
        url: gBASE_URL + "province/" + paramId,
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            handleDistrict(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onLoadWard(paramId) {
    $.ajax({
        url: gBASE_URL + "district/" + paramId,
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            handleWard(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}

function onLoadFilterByPrice(paramPrice1, paramPrice2, vPage) {
    $.ajax({
        url: gBASE_URL + "filter/realestate/price/" + paramPrice1 + "/" + paramPrice2 + "?page=" + vPage + "&size=3",
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {

            loadProperties(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onLoadFilterByAcreage(paramAc1, paramAc2, vPage) {
    $.ajax({
        url: gBASE_URL + "filter/realestate/acreage/" + paramAc1 + "/" + paramAc2 + "?page=" + vPage + "&size=3",
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {

            loadProperties(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onLoadFilterByType(paramType) {
    $.ajax({
        url: gBASE_URL + "filter/realestate/type/" + paramType,
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {

            loadProperties(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onLoadFilterByRequest(paramType) {
    $.ajax({
        url: gBASE_URL + "filter/realestate/request/" + paramType,
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {

            loadProperties(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onLoadFilterByAddress(paramAdd1, paramAdd2, paramAdd3, vPage) {
    $.ajax({
        url: gBASE_URL + "filter/realestate/address/" + paramAdd1 + "/" + paramAdd2 + "/" + paramAdd3 + "?page=" + vPage + "&size=3",
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {

            loadProperties(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function onCLick() {
    $("#buy").on("click", function () {
        $("#buy").addClass("clicked");
    })

}
function handleProvince(paramData) {
    for (var bI = 0; bI < paramData.length; bI++) {
        $("#select-province").append($("<option>", {
            text: paramData[bI].name,
            value: paramData[bI].id
        }))
    }

}
function handleDistrict(paramData) {
    for (var bI = 0; bI < paramData.districts.length; bI++) {
        $("#select-district").append($("<option>", {
            text: paramData.districts[bI].name,
            value: paramData.districts[bI].id
        }))
    }

}
function handleWard(paramData) {
    for (var bI = 0; bI < paramData.wards.length; bI++) {
        $("#select-ward").append($("<option>", {
            text: paramData.wards[bI].name,
            value: paramData.wards[bI].id
        }))
    }

}
function findAddress() {
    var vProvince = $('#select-province :selected').text();
    var vDistrict = $('#select-district :selected').text();
    var vWard = $('#select-ward :selected').text();
    var vAddress = vProvince + "-" + vDistrict + "-" + vWard;
    var chuoiTimKiem = vAddress;

    var ketQua = vData.filter(function (item) {
        return item.address.includes(chuoiTimKiem);
    });
    return ketQua;



}
function onClickPagin(a, param1, param2, param3) {
    $("#row-realestate").html("");
    $("#pagin-real-main").hide();
    $("#pagin-ac").show();

    $("#pagin-ac3").on("click", function () {

        $("#pagin-ac1").removeClass("active");
        $("#pagin-ac2").removeClass("active");
        $("#pagin-ac3").addClass("active");
        $("#row-realestate").html("");
        if (a == "ac") {
            onLoadFilterByAcreage(param1, param2, 2);
        }
        else if (a == "price") {
            onLoadFilterByPrice(param1, param2, 2);
        }
        else if (a == "address") {
            $("#row-realestate").html("");
            onLoadFilterByAddress(param1, param2, param3, 2);
        }


    })
    $("#pagin-ac2").on("click", function () {
        $("#row-realestate").html("");
        $("#pagin-ac1").removeClass("active");
        $("#pagin-ac2").addClass("active");
        $("#pagin-ac3").removeClass("active");

        if (a == "ac") {
            $("#row-realestate").html("");
            onLoadFilterByAcreage(param1, param2, 1);
            $("#row-realestate").html("");
        }
        else if (a == "price") {
            $("#row-realestate").html("");
            onLoadFilterByPrice(param1, param2, 1);
            $("#row-realestate").html("");
        }
        else if (a == "address") {
            $("#row-realestate").html("");
            onLoadFilterByAddress(param1, param2, param3, 1);
        }

    })
    $("#pagin-ac1").on("click", function () {

        $("#row-realestate").html("");
        $("#pagin-ac1").addClass("active");
        $("#pagin-ac2").removeClass("active");
        $("#pagin-ac3").removeClass("active");

        if (a == "ac") {
            $("#row-realestate").html("");
            onLoadFilterByAcreage(param1, param2, 0);
            $("#row-realestate").html("");
        }
        else if (a == "price") {
            $("#row-realestate").html("");
            onLoadFilterByPrice(param1, param2, 0);
        }
        else if (a == "address") {
            $("#row-realestate").html("");
            onLoadFilterByAddress(param1, param2, param3, 0);
        }
    })
    $("#pagin-ac4").on("click", function () {
        $("#pagin-ac1").removeClass("active");
        $("#pagin-ac2").removeClass("active");
        $("#pagin-ac3").removeClass("active");
        $("#pagin-ac4").addClass("active");
        $("#row-realestate").html("");
        if (a == "ac") {
            onLoadFilterByAcreage(param1, param2, 3);
        }
        else if (a == "price") {
            onLoadFilterByPrice(param1, param2, 3);
        }
        else if (a == "address") {
            $("#row-realestate").html("");
            onLoadFilterByAddress(param1, param2, param3, 3);
        }
    })
}





