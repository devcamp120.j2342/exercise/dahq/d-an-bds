

"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/"
// Lấy giá trị user_id từ LocalStorage
var accessToken = localStorage.getItem('token');
var vId = localStorage.getItem('user_id');
console.log(accessToken);
console.log(vId);
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    $("#btn-save-add").on("click", function () {
        onBtnAdd(vId);
    })
    onLoadProvince();
    $("#select-province").on("change", function () {
        var vId = $("#select-province").val();
        $("#select-district").html("");
        onLoadDistrict(vId);
    })
    $("#select-district").on("change", function () {
        var vId = $("#select-district").val();
        $("#select-ward").html("");
        onLoadWard(vId);
    })

})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onBtnAdd(id) {
    var vObjProperties = {
        title: "",
        type: 0,
        request: 0,
        address: "",
        price: 0,
        priceMin: 0,
        priceTime: 0,
        dateCreate: "",
        acreage: 0,
        direction: 3,
        totalFloors: 0,
        numberFloors: 0,
        bath: 0,
        apartCode: "",
        wallArea: 0.0,
        bedroom: 0,
        balcony: 0,
        landscapeView: "",
        apartLoca: 0,
        partType: 0,
        furnitureType: 0,
        priceRent: 12,
        returnRate: 0.0,
        legalDoc: 0,
        description: "",
        withY: 0,
        longX: 0,
        streetHouse: 0,
        viewNum: 0,
        createBy: 0,
        updateBy: 0,
        shape: null,
        distance2facade: 0,
        adjacentFacadeNum: 0,
        adjacentRoad: null,
        alleyMinWidth: 0,
        adjacentAlleyMinWidth: 0,
        factor: 0,
        structure: null,
        photo: "",
        lat: 0.0,
        lng: 0.0,
        fsbo: 0,
        clcl: 0,
        dtsxd: 0,
        ctxdprice: 0,
        ctxdvalue: 0
    }
    var vIdPro = $("#select-province").val();
    var vIdDis = $("#select-district").val();
    var vIdWard = $("#select-ward").val();
    getInfoProperties(vObjProperties);
    $.ajax({
        url: gBASE_URL + "realestate/" + id + "/" + vIdPro + "/" + vIdDis + "/" + vIdWard,
        type: "POST",
        headers: { "Authorization": "Bearer " + accessToken },
        data: JSON.stringify(vObjProperties),
        datatype: "application/json",
        contentType: "application/json; charset=utf-8",
        success: function (paramRes) {
            var vId = paramRes.id;
            alert("Post Pending of Admin");
            window.location.href = "http://localhost:5501/AddPhoto.html?id=" + vId;
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })


}
function onLoadProvince() {
    $.ajax({
        url: gBASE_URL + "province",
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            handleProvince(paramRes);
            // console.log(accessToken)
            // alert("hello");
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onLoadDistrict(paramId) {
    $.ajax({
        url: gBASE_URL + "province/" + paramId,
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            handleDistrict(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}
function onLoadWard(paramId) {
    $.ajax({
        url: gBASE_URL + "district/" + paramId,
        type: "GET",
        headers: { "Authorization": "Bearer " + accessToken },
        success: function (paramRes) {
            handleWard(paramRes);
        },
        error: function (paramErr) {
            alert(paramErr.status);
        }
    })
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function getInfoProperties(paramObj) {
    paramObj.title = $("#inp-title").val();
    paramObj.partType = $("#select-type").val();
    paramObj.description = $("#inp-des").val();
    var vPrice = $('#select-price :selected').text();
    let result = vPrice.replace("tỷ", "");
    console.log(result); // Kết quả: "1000"
    paramObj.price = result;
    paramObj.address = $("#inp-address").val();
    paramObj.apartCode = $("#inp-code").val();
    paramObj.request = $("#select-status").val();

    paramObj.acreage = $("#inp-area").val();
    paramObj.bedroom = $("#inp-bed").val();
    paramObj.bath = $("#inp-bath").val();
    var filePath = $("#inp-file").val();
    var fileName = filePath.replace(/^.*\\/, "");

    paramObj.photo = fileName;


}
function handleProvince(paramData) {
    for (var bI = 0; bI < paramData.length; bI++) {
        $("#select-province").append($("<option>", {
            text: paramData[bI].name,
            value: paramData[bI].id
        }))
    }

}
function handleDistrict(paramData) {
    for (var bI = 0; bI < paramData.districts.length; bI++) {
        $("#select-district").append($("<option>", {
            text: paramData.districts[bI].name,
            value: paramData.districts[bI].id
        }))
    }

}
function handleWard(paramData) {
    for (var bI = 0; bI < paramData.wards.length; bI++) {
        $("#select-ward").append($("<option>", {
            text: paramData.wards[bI].name,
            value: paramData.wards[bI].id
        }))
    }

}
